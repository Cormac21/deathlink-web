package com.cormacx.deathlink.deathlinkweb.rest.contact;

import java.util.List;

import lombok.Data;

@Data
public class GetContactsEmbedded {

	private List<Contact> contactModelList;
	
}
