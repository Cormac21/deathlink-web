package com.cormacx.deathlink.deathlinkweb.rest.person;

import java.time.LocalDate;

import org.springframework.hateoas.Links;

import lombok.Data;

@Data
public class Person {

	private String name;
	
	private LocalDate birthday;
	
	private Integer ageInYears;
	
	private String profession;
	
	private String dadsName;
	
	private String momsName;
	
	private Boolean isDeceased;
	
	private Long deathId;
	
	private Links links;
	
}
