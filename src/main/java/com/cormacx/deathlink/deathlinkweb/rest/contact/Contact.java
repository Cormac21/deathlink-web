package com.cormacx.deathlink.deathlinkweb.rest.contact;

import org.springframework.hateoas.Links;
import org.springframework.hateoas.RepresentationModel;

import lombok.Data;

@Data
public class Contact extends RepresentationModel<Contact>{
	
	private String name;
	
	private String type;
	
	private String phoneNumber;
	
	private String email;
	
	private boolean contactInCaseOfDeath;
	
}
