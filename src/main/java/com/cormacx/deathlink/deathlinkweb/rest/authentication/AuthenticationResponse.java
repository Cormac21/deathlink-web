package com.cormacx.deathlink.deathlinkweb.rest.authentication;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class AuthenticationResponse {

	@Getter
	private String jwt;
	
}
