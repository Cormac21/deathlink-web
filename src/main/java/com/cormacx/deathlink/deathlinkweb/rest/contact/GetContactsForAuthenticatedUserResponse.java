package com.cormacx.deathlink.deathlinkweb.rest.contact;

import org.springframework.hateoas.RepresentationModel;

import lombok.Data;

@Data
public class GetContactsForAuthenticatedUserResponse extends RepresentationModel<GetContactsForAuthenticatedUserResponse>{
	
	private GetContactsEmbedded _embedded;

}
