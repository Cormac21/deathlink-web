package com.cormacx.deathlink.deathlinkweb.rest.death;

import java.time.LocalDate;

import org.springframework.hateoas.Links;

import com.cormacx.deathlink.deathlinkweb.rest.person.Person;

import lombok.Data;

@Data
public class Death {

	private LocalDate dateOfDeath;
	
	private String placeOfDeath;
	
	private String placeOfFuneral;
	
	private String placeOfBurial;
	
	private LocalDate dateOfBurial;
	
	private String funerary;
	
	private Person person;
	
	private Links links;
	
}
