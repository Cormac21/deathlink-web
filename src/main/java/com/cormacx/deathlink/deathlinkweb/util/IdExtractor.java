package com.cormacx.deathlink.deathlinkweb.util;

import org.springframework.stereotype.Component;

@Component
public class IdExtractor {

	public static String extrairIdDeLink( String link ) {
		return link.substring(link.lastIndexOf('/')+1, link.length());
	}
	
}
