package com.cormacx.deathlink.deathlinkweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.config.HypermediaRestTemplateConfigurer;

@SpringBootApplication
public class DeathlinkWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeathlinkWebApplication.class, args);
	}

	@Bean
	RestTemplateCustomizer hypermediaRestTemplateCustomizer(HypermediaRestTemplateConfigurer configurer) { 
	    return restTemplate -> { 
	        configurer.registerHypermediaTypes(restTemplate); 
	    };
	}
	
}
