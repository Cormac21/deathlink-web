package com.cormacx.deathlink.deathlinkweb.client;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.client.Traverson;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.cormacx.deathlink.deathlinkweb.rest.authentication.AuthenticationRequest;
import com.cormacx.deathlink.deathlinkweb.rest.authentication.AuthenticationResponse;
import com.cormacx.deathlink.deathlinkweb.rest.contact.Contact;
import com.cormacx.deathlink.deathlinkweb.rest.contact.GetContactsForAuthenticatedUserResponse;
import com.cormacx.deathlink.deathlinkweb.rest.death.Death;
import com.cormacx.deathlink.deathlinkweb.util.DeathlinkConstants;

@Component
public class DeathlinkServiceClient {
	
	private static Logger logger = LoggerFactory.getLogger(DeathlinkServiceClient.class);
	
	@Value("${deathlink.base.url}")
	String deathlinkUrl;
	
	private RestTemplate client ;
	
	@Autowired
	public DeathlinkServiceClient( RestTemplateBuilder builder ) {
		this.client = builder.build();
	}
	
	public AuthenticationResponse postAuthenticate(String username, String password) {
		AuthenticationRequest request = new AuthenticationRequest(username, password);
		ResponseEntity<AuthenticationResponse> response = client.postForEntity(deathlinkUrl.concat(DeathlinkConstants.AUTHENTICATE), request, AuthenticationResponse.class);
		return response.getBody();
	}
	
	public List<Death> getAllDeaths(Integer pageNumber, Integer pageSize, String sortBy, String jwt) {
		HttpEntity<String> request = createHttpEntity(jwt);
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(deathlinkUrl.concat(DeathlinkConstants.DEATHS))
				.queryParam("pageNumber", pageNumber)
				.queryParam("pageSize", pageSize)
				.queryParam("sortBy", sortBy);
		try {
			ResponseEntity<Death[]> response = client.exchange(
					builder.toUriString(), 
					HttpMethod.GET, 
					request
					, Death[].class);
			logger.info(response.toString());
			return Arrays.asList(response.getBody());
		} catch (Exception e) {
			logger.error("", e);
		}
		return null;
	}
	
	public Death findDeathById(Integer i, String jwt) {
		HttpEntity<String> request = createHttpEntity(jwt);
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(deathlinkUrl.concat(DeathlinkConstants.DEATHS)+'/'+i);
		
		try {
			ResponseEntity<Death> response = client.exchange(
					builder.toUriString(),
					HttpMethod.GET,
					request,
					Death.class);
			logger.info(response.getBody().toString());
			return response.getBody();
		} catch (Exception e) {
			logger.error("", e);
		}
		return null;
	}
	
	public List<Contact> getContactsForAuthenticatedUser(String jwt) {
		HttpEntity<String> request = createHttpEntity(jwt);
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(deathlinkUrl.concat(DeathlinkConstants.CONTACTS));

		try {
			ResponseEntity<GetContactsForAuthenticatedUserResponse> response = client.exchange(
					builder.toUriString(),
					HttpMethod.GET,
					request,
					GetContactsForAuthenticatedUserResponse.class);
			logger.info(response.getBody().toString());
			return response.getBody().get_embedded().getContactModelList();
		} catch (Exception e) {
			logger.error("", e);
		}
		return null;
	}
	
	public Contact getContactByIdForAuthenticatedUser( Long i, String jwt ) {
		HttpEntity<String> request = createHttpEntity(jwt);
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(deathlinkUrl.concat(DeathlinkConstants.CONTACTS)+'/'+i);
		
		try {
			ResponseEntity<Contact> response = client.exchange(
					builder.toUriString(),
					HttpMethod.GET,
					request,
					Contact.class);
			logger.info(response.getBody().toString());
			return response.getBody();
		} catch (Exception e) {
			logger.error("", e);
		}
		return null;
	}
	
	public Contact saveContactForAuthenticatedUser(Contact newContact, String jwt) {
		HttpEntity<Object> request = createHttpJsonEntity(newContact, jwt);
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(deathlinkUrl.concat(DeathlinkConstants.CONTACTS));
		
		try {
			ResponseEntity<Contact> response = client.exchange(
					builder.toUriString(),
					HttpMethod.POST,
					request,
					Contact.class);
			logger.info(response.getBody().toString());
			return response.getBody();
		} catch (Exception e) {
			logger.error("", e);
		}
		return null;
	}
	
	public String deleteContactByIdForAuthenticatedUser(String extrairIdDeLink, String jwt) {
		HttpEntity<String> request = createHttpEntity(jwt);
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(deathlinkUrl.concat(DeathlinkConstants.CONTACTS));
		
		try {
			ResponseEntity<String> response = client.exchange(
					builder.toUriString(),
					HttpMethod.DELETE,
					request,
					String.class);
			logger.info(response.getBody().toString());
			return response.getBody();
		} catch (Exception e) {
			logger.error("", e);
		}
		return null;
	}

	private HttpEntity<String> createHttpEntity(String jwt) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaTypes.HAL_JSON_VALUE);
		setAuthentication(headers, jwt);
		return new HttpEntity<>(headers);
	}
	
	private HttpEntity<Object> createHttpJsonEntity(Object body, String jwt) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Accept", MediaTypes.HAL_JSON_VALUE);
		setAuthentication(headers, jwt);
		return new HttpEntity<Object>(body, headers);
	}
	
	public void setAuthentication(HttpHeaders headers, String jwt) {
		headers.add("Authorization", "Bearer "+jwt);
	}

}
