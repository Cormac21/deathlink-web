package com.cormacx.deathlink.deathlinkweb;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.web.Link;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.LinkRelation;

import com.cormacx.deathlink.deathlinkweb.client.DeathlinkServiceClient;
import com.cormacx.deathlink.deathlinkweb.rest.authentication.AuthenticationResponse;
import com.cormacx.deathlink.deathlinkweb.rest.contact.Contact;
import com.cormacx.deathlink.deathlinkweb.rest.death.Death;
import com.cormacx.deathlink.deathlinkweb.util.IdExtractor;

@SpringBootTest
public class DeathlinkServiceClientTest {
	
	@Autowired
	DeathlinkServiceClient deathlinkServiceClient;
	
	String authenticationToken = null;
	
	@BeforeEach
	public void init() {
		AuthenticationResponse response = deathlinkServiceClient.postAuthenticate("test@test.com", "password");
		authenticationToken = response.getJwt();
	}
	
	@Test
	public void postAuthenticate_returns_200() {
		deathlinkServiceClient.postAuthenticate("test@test.com", "password");
	}
	
	@Test
	public void getListOfDeaths_returns_200() {
		List<Death> deaths = deathlinkServiceClient.getAllDeaths(0, 10, null, authenticationToken);
	}
	
	@Test
	public void getDeathById_returns_200() {
		Death death = deathlinkServiceClient.findDeathById( 1, authenticationToken );
	}
	
	@Test
	public void getContactsForAuthenticatedUser_returns_200() {
		List<Contact> contact = deathlinkServiceClient.getContactsForAuthenticatedUser( authenticationToken );
	}
	
	@Test
	public void getContactByIdForAuthenticatedUser_returns_200() {
		Contact contact = deathlinkServiceClient.getContactByIdForAuthenticatedUser( 1L, authenticationToken);
	}
	
	@Test
	public void saveContactForAuthenticatedUser_returns_200() {
		Contact newContact = new Contact();
		newContact.setEmail("phonyemail@test.com");
		newContact.setType("email");
		newContact.setName("ASKDJqowieuknwqg");
		newContact.setContactInCaseOfDeath(false);
		deathlinkServiceClient.saveContactForAuthenticatedUser( newContact, authenticationToken );
	}
	
	@Test
	public void deleteContactForAuthenticatedUser_returns_200() {
		saveContactForAuthenticatedUser_returns_200();
		List<Contact> contacts = deathlinkServiceClient.getContactsForAuthenticatedUser( authenticationToken );
		
		deathlinkServiceClient.deleteContactByIdForAuthenticatedUser(
				IdExtractor.extrairIdDeLink(contacts.get(0).getLinks().getLink("self").toString()),
				authenticationToken);
	}
	
	@Test
	public void getCompanies_returns_200() {
		
	}
	
	@Test
	public void getServices_returns_200() {
		
	}
	
	@Test
	public void getServicesForCompanyById_returns_200() {
		
	}
	
}
